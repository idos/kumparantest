# KumparanTest

This apps showing list Post from API https://jsonplaceholder.typicode.com/. Source code using swift 5 and SwiftUi.

- SwiftUI MVVM & Unit Test
- Load data from server using URLSession
- Showing list post from https://jsonplaceholder.typicode.com/
- Navigate to detail page
- Load image from url using asyncimage from native swiftui
