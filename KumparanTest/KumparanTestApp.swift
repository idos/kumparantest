//
//  KumparanTestApp.swift
//  KumparanTest
//
//  Created by R+IOS on 21/02/22.
//

import SwiftUI

@main
struct KumparanTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
