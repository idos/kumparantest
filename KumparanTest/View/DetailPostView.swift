//
//  DetailPostView.swift
//  KumparanTest
//
//  Created by R+IOS on 22/02/22.
//

import SwiftUI

struct DetailPostView: View {
    @StateObject var viewModel = DetailPostViewModel()
    
    let user: User
    let post: Post
    
    var body: some View {
        GeometryReader { geometry in
            VStack(alignment: .leading, spacing: 10) {
                NavigationLink {
                    UserDetailPageView(user: self.user)
                } label: {
                    Text("User Name: \(user.name)")
                        .foregroundColor(.blue)
                }

                
                Text("Title: \(post.title)")
                
                Text("Body: \(post.body)")
                
                if viewModel.comments.count > 0 {
                    List(viewModel.comments, id: \.id) { comment in
                        CommentView(comment: comment)
                            .padding([.all], 0)
                    }
                    .padding(.top, 10)
                }
            }
            .navigationBarTitle(Text("Detail Post"))
            .onAppear {
                viewModel.loadComment(userId: user.id)
            }
        }
    }
}

//struct DetailPostView_Previews: PreviewProvider {
//    static var previews: some View {
//        DetailPostView(user: <#User#>, post: <#Post#>)
//    }
//}
