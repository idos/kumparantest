//
//  UserDetailPageView.swift
//  KumparanTest
//
//  Created by R+IOS on 22/02/22.
//

import SwiftUI

struct UserDetailPageView: View {
    @StateObject var viewModel = UserViewModel()
    
    let user: User
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            if viewModel.loading {
                ProgressView()
            } else {
                Text("User Name: \(user.name)")
                
                Text("Email: \(user.email)")
                
                Text("Address: \(user.address.street)" +  ", \(user.address.suite)" + ", \(user.address.city)")
                
                Text("Company: \(user.company.name)")
                
                if viewModel.albums.count > 0 {
                    List(viewModel.albums, id: \.id) { album in
                        AlbumView(album: album)
                    }
                    .padding([.leading, .trailing], -20)
                }
            }
        }
        .navigationBarTitle(Text("User"))
        .onAppear {
            viewModel.loadAlbums(userId: user.id)
        }
    }
}

//struct UserDetailPageView_Previews: PreviewProvider {
//    static var previews: some View {
//        UserDetailPageView()
//    }
//}
