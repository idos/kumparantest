//
//  AlbumViews.swift
//  KumparanTest
//
//  Created by R+IOS on 22/02/22.
//

import SwiftUI

struct AlbumView: View {
    @StateObject var viewModel = ALbumViewModel()
    
    let album: Album
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Text("Album Name: \(album.title)")
            
            if viewModel.photos.count > 0 {
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack(spacing: 20) {
                        ForEach(viewModel.photos, id: \.id) { photo in
                            NavigationLink {
                                DetailPhoto(photo: photo)
                            } label: {
                                PhotoRow(photo: photo)
                            }
                        }
                    }
                }
            }
        }
        .onAppear {
            self.viewModel.loadPhotos(albumId: album.id)
        }
    }
}

struct AlbumViews_Previews: PreviewProvider {
    static var previews: some View {
        AlbumView(album: Album.with())
    }
}
