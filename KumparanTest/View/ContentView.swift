//
//  ContentView.swift
//  KumparanTest
//
//  Created by R+IOS on 21/02/22.
//

import SwiftUI

struct ContentView: View {
    @StateObject var viewModel = PostViewModel()
    
    var body: some View {
        NavigationView {
            VStack(alignment: .center) {
                if viewModel.loading {
                    ProgressView()
                } else {
                    if (viewModel.posts.count > 0 && viewModel.users.count > 0) {
                        List(viewModel.posts, id: \.id) { post in
                            let user = self.viewModel.getUserById(userId: post.userId)

                            NavigationLink {
                                DetailPostView(user: user, post: post)
                            } label: {
                                PostView(post: post, user: user)
                            }

                        }
                    } else {
                        VStack(alignment: .center) {
                            Text("No post ies or error")
                        }
                    }
                }
            }
            .onAppear {
                self.viewModel.loadPosts()
                self.viewModel.loadUsers()
            }
            .navigationBarTitle(Text("Post"))
        }
        .background(.white)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
