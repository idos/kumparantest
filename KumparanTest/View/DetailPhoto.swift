//
//  DetailPhoto.swift
//  KumparanTest
//
//  Created by R+IOS on 22/02/22.
//

import SwiftUI

struct DetailPhoto: View {
    @State private var zoomlevel: CGFloat = 1
    
    let photo: Photo
    
    var body: some View {
        VStack(alignment: .center, spacing: 10) {
            Text("Title: \(photo.title)")
            
            AsyncImage(
                url: URL(string: photo.thumbnailUrl),
                content: { image in
                    image.resizable()
                        .aspectRatio(contentMode: .fill)
                        .scaleEffect(self.zoomlevel)
                        .gesture(MagnificationGesture().onChanged({ (value) in
                            self.zoomlevel = value.magnitude
                        }))
                },
                placeholder: {
                    ProgressView()
                }
            )
        }
    }
}

struct DetailPhoto_Previews: PreviewProvider {
    static var previews: some View {
        DetailPhoto(photo: Photo.with())
    }
}
