//
//  PostListView.swift
//  KumparanTest
//
//  Created by R+IOS on 22/02/22.
//

import Foundation
import SwiftUI

struct PostView: View {
    var post: Post
    var user: User
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Spacer()
            
            Text("Title: \(self.post.title)")
                .font(.callout)
                .foregroundColor(.gray)
                .lineLimit(nil)
            
            Text("Body: \(self.post.body)")
                .font(.callout)
                .foregroundColor(.gray)
                .lineLimit(nil)
            
            Text("User: \(self.user.username)")
                .font(.callout)
                .foregroundColor(.gray)
                .lineLimit(0)
            
            Text("Company: \(self.user.company.name)")
                .font(.callout)
                .foregroundColor(.gray)
                .lineLimit(0)
            
            Spacer()
        }
        .padding([.trailing], 10)
    }
}
