//
//  PhotoRow.swift
//  KumparanTest
//
//  Created by R+IOS on 22/02/22.
//

import SwiftUI

struct PhotoRow: View {
    let photo: Photo
    
    var body: some View {
        AsyncImage(
            url: URL(string: photo.thumbnailUrl),
            content: { image in
                image.resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(maxWidth: 80, maxHeight: 80)
            },
            placeholder: {
                ProgressView()
            }
        )
    }
}

struct PhotoRow_Previews: PreviewProvider {
    static var previews: some View {
        PhotoRow(photo: Photo.with())
    }
}
