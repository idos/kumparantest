//
//  CommentView.swift
//  KumparanTest
//
//  Created by R+IOS on 22/02/22.
//

import SwiftUI

struct CommentView: View {
    var comment: Comment
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10){
            Spacer()
            
            Text("Comment body: \(comment.body)")
                .lineLimit(nil)
            
            Text("Author: \(comment.name)")
            
            Spacer()
        }
    }
}

//struct CommentView_Previews: PreviewProvider {
//    static var previews: some View {
//        CommentView()
//    }
//}
