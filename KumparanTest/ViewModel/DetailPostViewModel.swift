//
//  DetailPostViewModel.swift
//  KumparanTest
//
//  Created by R+IOS on 22/02/22.
//

import Foundation

class DetailPostViewModel: ObservableObject {
    @Published var comments: [Comment] = []
    @Published var loading: Bool = false
    
    let service: ServiceProtocol
    init(service: ServiceProtocol = APIService()) {
        self.service = service
    }
    
    func loadComment(userId: Int) {
        self.loading = true
        
        service.fetchComment(postId: userId) { comments in
            self.loading = false
            guard let comments = comments else {
                return
            }
            
            self.comments = comments
        }
    }
}
