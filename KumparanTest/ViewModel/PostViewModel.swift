//
//  PostViewModel.swift
//  KumparanTest
//
//  Created by R+IOS on 22/02/22.
//

import Foundation
import SwiftUI

class PostViewModel: ObservableObject {
    @Published var posts: [Post] = []
    @Published var users: [User] = []
    @Published var loading = false
    
    let service: ServiceProtocol
    init(service: ServiceProtocol = APIService()) {
        self.service = service
    }
    
    func loadPosts() {
        loading = true
        service.fetchPost { posts in
            self.loading = false
            guard let posts = posts else {
                return
            }
            
            self.posts = posts
        }
    }
    
    func loadUsers() {
        loading = true
        service.fetchUsers { users in
            self.loading = false
            guard let users = users else {
                return
            }
            
            self.users = users
        }
    }
    
    func getUserById(userId: Int) -> User {
        let result = self.users.first{ $0.id == userId }
        
        return result!
    }
    
}
