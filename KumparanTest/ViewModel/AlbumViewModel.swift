//
//  AlbumViewModel.swift
//  KumparanTest
//
//  Created by R+IOS on 22/02/22.
//

import Foundation

class ALbumViewModel: ObservableObject {
    @Published var photos: [Photo] = []
    @Published var loading: Bool = false
    
    let service: ServiceProtocol
    init(service: ServiceProtocol = APIService()) {
        self.service = service
    }
    
    func loadPhotos(albumId: Int) {
        self.loading = true
        
        service.fetchPhoto(albumId: albumId) { photos in
            self.loading = false
            guard let photos = photos else {
                return
            }
            
            self.photos = photos
        }
    }
}
