//
//  UserViewModel.swift
//  KumparanTest
//
//  Created by R+IOS on 22/02/22.
//

import Foundation

class UserViewModel: ObservableObject {
    @Published var albums: [Album] = []
    @Published var loading: Bool = false
    
    let service: ServiceProtocol
    init(service: ServiceProtocol = APIService()) {
        self.service = service
    }
    
    func loadAlbums(userId: Int) {
        self.loading = true
        
        service.fetchAlbum(userId: userId) { albums in
            self.loading = false
            guard let albums = albums else {
                return
            }
            
            self.albums = albums
        }
    }
}
