//
//  Photo.swift
//  KumparanTest
//
//  Created by R+IOS on 22/02/22.
//

import Foundation


struct Photo: Codable {
    var albumId: Int
    var id: Int
    var title: String
    var url: String
    var thumbnailUrl: String
}
