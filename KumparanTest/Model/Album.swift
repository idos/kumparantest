//
//  Album.swift
//  KumparanTest
//
//  Created by R+IOS on 22/02/22.
//

import Foundation

struct Album: Codable {
    var userId: Int
    var id: Int
    var title: String
}
