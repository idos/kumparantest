//
//  Comment.swift
//  KumparanTest
//
//  Created by R+IOS on 22/02/22.
//

import Foundation

struct Comment: Codable {
    var postId: Int
    var id: Int
    var name: String
    var email: String
    var body: String
}
