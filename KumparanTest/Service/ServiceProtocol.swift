//
//  ServiceProtocol.swift
//  KumparanTest
//
//  Created by R+IOS on 22/02/22.
//

import Foundation

protocol ServiceProtocol {
    func fetchPost(completion: @escaping([Post]?) -> Void)
    func fetchComment(postId: Int, completion: @escaping([Comment]?) -> Void)
    func fetchAlbum( userId: Int, completion: @escaping([Album]?) -> Void)
    func fetchPhoto(albumId: Int, completion: @escaping([Photo]?) -> Void)
    func fetchUsers(completion: @escaping([User]?) -> Void)
}
