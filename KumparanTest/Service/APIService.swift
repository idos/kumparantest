//
//  APIService.swift
//  KumparanTest
//
//  Created by R+IOS on 22/02/22.
//

import Foundation

class APIService: ServiceProtocol {
    
    func fetchPost(completion: @escaping ([Post]?) -> Void) {
        let url = URL(string: "https://jsonplaceholder.typicode.com/posts")!
        
        self.fetchData(with: url) { data in
            guard let data = data else {
                completion(nil)
                return
            }
            guard let posts = try? JSONDecoder().decode([Post].self, from: data) else {
                completion(nil)
                return
            }
            DispatchQueue.main.async {
                completion(posts)
            }
        }
    }
    
    func fetchComment(postId: Int, completion: @escaping ([Comment]?) -> Void) {
        let url = URL(string: "https://jsonplaceholder.typicode.com/comments?postId=\(postId)")!
        
        self.fetchData(with: url) { data in
            guard let data = data else {
                completion(nil)
                return
            }
            
            guard let comments = try? JSONDecoder().decode([Comment].self, from: data) else {
                completion(nil)
                return
            }
            
            DispatchQueue.main.async {
                completion(comments)
            }
        }
    }
    
    func fetchAlbum(userId: Int, completion: @escaping ([Album]?) -> Void) {
        let url = URL(string: "https://jsonplaceholder.typicode.com/users/\(userId)/albums")!
        
        self.fetchData(with: url) { data in
            guard let data = data else {
                completion(nil)
                return
            }
            
            guard let albums = try? JSONDecoder().decode([Album].self, from: data) else {
                completion(nil)
                return
            }
            
            DispatchQueue.main.async {
                completion(albums)
            }
        }
    }
    
    func fetchPhoto(albumId: Int, completion: @escaping ([Photo]?) -> Void) {
        let url = URL(string: "https://jsonplaceholder.typicode.com/albums/\(albumId)/photos")!
        
        self.fetchData(with: url) { data in
            guard let data = data else {
                completion(nil)
                return
            }
            
            guard let photos = try? JSONDecoder().decode([Photo].self, from: data) else {
                completion(nil)
                return
            }
            
            DispatchQueue.main.async {
                completion(photos)
            }
        }
    }

    func fetchUsers(completion: @escaping ([User]?) -> Void) {
        let url = URL(string: "https://jsonplaceholder.typicode.com/users")!
        
        self.fetchData(with: url) { data in
            guard let data = data else {
                return
            }
            
            guard let users = try? JSONDecoder().decode([User].self, from: data) else {
                return
            }
            
            DispatchQueue.main.async {
                completion(users)
            }
        }
    }
    
    private func fetchData(with url: URL, completion: @escaping (Data?) -> Void) {
        URLSession.shared.dataTask(with: url) { data, _, _ in
            guard let data = data else {
                completion(nil)
                return
            }
            
            DispatchQueue.main.async {
                completion(data)
            }
        }.resume()
    }
}
