//
//  MockService.swift
//  KumparanTestTests
//
//  Created by R+IOS on 22/02/22.
//

import Foundation

class MockService: ServiceProtocol {
    let mockPosts: [Post]?
    let mockComments: [Comment]?
    let mockAlbums: [Album]?
    let mockPhotos: [Photo]?
    let mockUser: [User]?
    
    init(
        mockPosts: [Post] = [Post.with()],
        mockComments: [Comment] = [Comment.with()],
        mockAlbums: [Album] = [Album.with()],
        mockPhotos: [Photo] = [Photo.with()],
        mockUser: [User] = [User.with()]
    ) {
        self.mockPosts = mockPosts
        self.mockComments = mockComments
        self.mockAlbums = mockAlbums
        self.mockPhotos = mockPhotos
        self.mockUser = mockUser
    }
    
    func fetchPost(completion: @escaping ([Post]?) -> Void) {
        completion(mockPosts)
    }
    
    func fetchComment(postId: Int, completion: @escaping ([Comment]?) -> Void) {
        completion(mockComments)
    }
    
    func fetchAlbum(userId: Int, completion: @escaping ([Album]?) -> Void) {
        completion(mockAlbums)
    }
    
    func fetchPhoto(albumId: Int, completion: @escaping ([Photo]?) -> Void) {
        completion(mockPhotos)
    }
    
    func fetchUsers(completion: @escaping ([User]?) -> Void) {
        completion(mockUser)
    }
    
}
