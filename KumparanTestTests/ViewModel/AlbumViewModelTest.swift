//
//  AlbumViewModelTest.swift
//  KumparanTestTests
//
//  Created by R+IOS on 22/02/22.
//

import XCTest

class AlbumViewModelTest: XCTestCase {

    func testLoadPhotos() {
        let service = MockService()
        let viewModel = ALbumViewModel(service: service)
        
        viewModel.loadPhotos(albumId: Album.with().id)
        
        XCTAssertTrue(!viewModel.loading)
        XCTAssertEqual(viewModel.photos.count, 1)
    }

}
