//
//  PostViewModelTest.swift
//  KumparanTestTests
//
//  Created by R+IOS on 22/02/22.
//

import XCTest
@testable import KumparanTest

class PostViewModelTest: XCTestCase {
    
    func testLoadPosts() {
        let service = MockService()
        let viewModel = PostViewModel(service: service)
        
        viewModel.loadPosts()
        
        XCTAssertTrue(!viewModel.loading)
        XCTAssertEqual(viewModel.posts.count, 1)
    }
    
    func testLoadUsers() {
        let service = MockService()
        let viewModel = PostViewModel(service: service)
        
        viewModel.loadUsers()
        
        XCTAssertTrue(!viewModel.loading)
        XCTAssertEqual(viewModel.users.count, 1)
    }
    
}
