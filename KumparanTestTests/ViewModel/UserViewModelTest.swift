//
//  UserViewModelTest.swift
//  KumparanTestTests
//
//  Created by R+IOS on 22/02/22.
//

import XCTest

class UserViewModelTest: XCTestCase {

    func testLoadAlbums() {
        let service = MockService()
        let viewModel = UserViewModel(service: service)
        
        viewModel.loadAlbums(userId: User.with().id)
        
        XCTAssertTrue(!viewModel.loading)
        XCTAssertEqual(viewModel.albums.count, 1)
    }

}
