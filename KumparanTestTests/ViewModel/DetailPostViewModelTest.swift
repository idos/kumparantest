//
//  DetailPostViewModelTest.swift
//  KumparanTestTests
//
//  Created by R+IOS on 22/02/22.
//

import XCTest

class DetailPostViewModelTest: XCTestCase {

    func testLoadComment() {
        let service = MockService()
        let viewModel = DetailPostViewModel(service: service)
        
        viewModel.loadComment(userId: User.with().id)
        
        XCTAssertTrue(!viewModel.loading)
        XCTAssertEqual(viewModel.comments.count, 1)
    }

}
