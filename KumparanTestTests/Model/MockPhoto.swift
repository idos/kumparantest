//
//  MockPhoto.swift
//  KumparanTestTests
//
//  Created by R+IOS on 22/02/22.
//

import Foundation

extension Photo {
    static func with(
        albumId: Int = 1,
        id: Int = 1,
        title: String = "accusamus beatae ad facilis cum similique qui sunt",
        url: String = "https://via.placeholder.com/600/92c952",
        thumbnailUrl: String = "https://via.placeholder.com/150/92c952"
    ) -> Photo {
        return Photo(albumId: albumId, id: id, title: title, url: url, thumbnailUrl: thumbnailUrl)
    }
}
