//
//  MockUser.swift
//  KumparanTestTests
//
//  Created by R+IOS on 22/02/22.
//

import Foundation

extension User {
    static func with(
        id: Int = 1,
        name: String = "Leanne Graham",
        username: String = "Bret",
        email: String = "Sincere@april.biz",
        address: Address = Address(
            street: "Kulas Light",
            suite: "Apt. 556",
            city: "Gwenborough",
            zipcode:  "92998-3874",
            geo: Geo(
                lat: "-37.3159",
                lng: "81.1496"
            )
        ),
        phone: String = "1-770-736-8031 x56442",
        website: String = "hildegard.org",
        company: Company = Company(
            name: "Romaguera-Crona",
            catchPhrase: "Multi-layered client-server neural-net",
            bs: "harness real-time e-markets"
        )
    ) -> User {
        return User(
            id: id,
            name: name,
            username: username,
            email: email,
            address: address,
            phone: phone,
            website: website,
            company: company
        )
    }
}
