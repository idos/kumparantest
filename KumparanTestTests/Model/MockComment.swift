//
//  MockComment.swift
//  KumparanTestTests
//
//  Created by R+IOS on 22/02/22.
//

import Foundation
import SwiftUI

extension Comment {
    static func with(
        postId: Int = 1,
        id: Int = 1,
        name: String = "id labore ex et quam laborum",
        email: String = "Eliseo@gardner.biz",
        body: String = "laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium"
    ) -> Comment {
        return Comment(postId: postId, id: id, name: name, email: email, body: body)
    }
}
