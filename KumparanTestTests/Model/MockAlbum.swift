//
//  MockAlbum.swift
//  KumparanTestTests
//
//  Created by R+IOS on 22/02/22.
//

import Foundation

extension Album {
    static func with(
        userId: Int = 1,
        id: Int = 1,
        title: String = "quidem molestiae enim"
    ) -> Album {
        return Album(userId: userId, id: id, title: title)
    }
}
