//
//  UserTest.swift
//  KumparanTestTests
//
//  Created by R+IOS on 22/02/22.
//

import XCTest

class UserTest: XCTestCase {

    func testSuccessParse() {
        let json = """
        {
            "id": 1,
            "name": "Leanne Graham",
            "username": "Bret",
            "email": "Sincere@april.biz",
            "address": {
              "street": "Kulas Light",
              "suite": "Apt. 556",
              "city": "Gwenborough",
              "zipcode": "92998-3874",
              "geo": {
                "lat": "-37.3159",
                "lng": "81.1496"
              }
            },
            "phone": "1-770-736-8031 x56442",
            "website": "hildegard.org",
            "company": {
              "name": "Romaguera-Crona",
              "catchPhrase": "Multi-layered client-server neural-net",
              "bs": "harness real-time e-markets"
            }
        }
        """.data(using: .utf8)!
        
        let user = try! JSONDecoder().decode(User.self, from: json)
        
        XCTAssertNotNil(user)
        XCTAssertEqual(user.id, User.with().id)
    }

}
