//
//  MockPost.swift
//  KumparanTestTests
//
//  Created by R+IOS on 22/02/22.
//

import Foundation

extension Post {
    static func with(
        userId: Int = 1,
        id: Int = 1,
        title: String =  "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        body: String =  "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
    ) -> Post {
        return Post(userId: userId, id: id, title: title, body: body)
    }
}
