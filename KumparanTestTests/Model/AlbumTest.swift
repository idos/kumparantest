//
//  AlbumTest.swift
//  KumparanTestTests
//
//  Created by R+IOS on 22/02/22.
//

import XCTest

class AlbumTest: XCTestCase {

    func testSuccessParse() {
        let json = """
        {
            "userId": 1,
            "id": 1,
            "title": "quidem molestiae enim"
        }
        """.data(using: .utf8)!
        
        let album = try! JSONDecoder().decode(Album.self, from: json)
        
        XCTAssertNotNil(album)
        XCTAssertEqual(album.id, Album.with().id)
    }

}
