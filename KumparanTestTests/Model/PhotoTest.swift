//
//  PhotoTest.swift
//  KumparanTestTests
//
//  Created by R+IOS on 22/02/22.
//

import XCTest

class PhotoTest: XCTestCase {

    func testSuccessParse() {
        let json = """
        {
            "albumId": 1,
            "id": 1,
            "title": "accusamus beatae ad facilis cum similique qui sunt",
            "url": "https://via.placeholder.com/600/92c952",
            "thumbnailUrl": "https://via.placeholder.com/150/92c952"
        }
        """.data(using: .utf8)!
        
        let photo = try! JSONDecoder().decode(Photo.self, from: json)
        
        XCTAssertNotNil(photo)
        XCTAssertEqual(photo.id, Photo.with().id)
    }

}
